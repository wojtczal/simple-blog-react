# Simple blog in React

To run first install dependencies:
```js
npm install
```
Then run the server:
```js
npm start
```

The application will be available at `http://localhost:8080`
