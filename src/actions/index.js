import * as types from './types';

export function createPost(post) {
  return {
    type: types.CREATE_POST,
    payload: post
  };
}

export function updatePost(post) {
  return {
    type: types.UPDATE_POST,
    payload: post
  }
}

export function deletePost(id) {
  return {
    type: types.DELETE_POST,
    payload: id
  }
}

export function updatePostFilter(filter) {
  return {
    type: types.UPDATE_FILTER,
    payload: filter
  }
}
