import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const HiddenActiveLink = (props) => {
  return (
    <Link {...props} activeStyle={{ display: 'none' }}>
      {props.children}
    </Link>
  );
};

HiddenActiveLink.propTypes = {
  to: PropTypes.string.isRequired
};

export default HiddenActiveLink;
