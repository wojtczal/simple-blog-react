import React from 'react';
import Navbar from './Navbar';

const MainLayout = (props) => {
  return (
    <div className="main-layout">
      <Navbar />
      {props.children}
    </div>
  );
};

export default MainLayout;
