import React from 'react';
import { Link } from 'react-router';
import HiddenActiveLink from './HiddenActiveLink';

const Navbar = () => {
  return (
    <nav className="navbar navbar-light bg-faded mb-1">
      <div className="nav navbar-nav">
        <Link className="nav-item nav-link" to="/">
          Home
        </Link>
        <HiddenActiveLink className="nav-item nav-link" to="/posts/new">
          Add Post
        </HiddenActiveLink>
      </div>
    </nav>
  );
};

export default Navbar;
