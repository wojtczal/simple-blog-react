import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const PostForm = (props) => {
  return (
    <form className="post-form col-xs-4" onSubmit={props.onSubmit}>
      <h4>{props.header}</h4>
      {props.children}
      <Link to="/" className="btn btn-danger">Cancel</Link>
      <button type="submit" className="btn btn-primary float-xs-right">Save</button>
    </form>
  );
};

PostForm.propTypes = {
  header: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default PostForm;
