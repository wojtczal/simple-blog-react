import React from 'react';
import PostListContainer from '../containers/PostListContainer';
import SearchFormContainer from '../containers/SearchFormContainer';

const PostIndex = () => {
  return (
    <div className="post-index">
      <SearchFormContainer />
      <PostListContainer />
    </div>
  );
};

export default PostIndex;
