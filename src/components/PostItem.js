import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const PostItem = ({id, title, onRemove}) => {
  return (
    <li className="post-item list-group-item clearfix">
      <strong className="align-middle">{title}</strong>
      <div className="float-xs-right">
        <Link to={`/posts/${id}/edit`}>
          <button className="btn btn-link">Edit</button>
        </Link>
        <button
          type="button"
          className="btn btn-danger"
          onClick={() => { onRemove(id) }} >
          Delete
        </button>
      </div>
    </li>
  )
}

PostItem.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  onRemove: PropTypes.func.isRequired
}

export default PostItem;
