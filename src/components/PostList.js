import React, { PropTypes } from 'react';
import PostItem from './PostItem';

const PostList = (props) => {
  return (
    <ul className="post-list list-group">
      {props.posts.map(post => {
        return (
          <PostItem
            key={post.id}
            onRemove={props.onPostRemove.bind(null, post.id)}
            {...post} />
        );
      })}
    </ul>
  );
};

PostList.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired
  })).isRequired,
  onPostRemove: PropTypes.func.isRequired
};

export default PostList;
