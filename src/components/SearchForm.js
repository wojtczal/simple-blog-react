import React, { PropTypes } from 'react';

const SearchForm = (props) => {
  return (
    <input
      type='search'
      className='search-form form-control mb-1'
      placeholder='Type to filter posts'
      value={props.value}
      onChange={props.onChange} />
  );
}

SearchForm.propTypes ={
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default SearchForm;
