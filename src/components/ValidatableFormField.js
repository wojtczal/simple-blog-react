import React, { PropTypes } from 'react';

const ValidatableFormField = ({ Field, input, label, type, meta: { touched, invalid, error} }) => {
  return (
    <div className={`form-group ${touched && invalid && 'has-danger'}`}>
      <label className="form-control-label">{label}</label>
      <Field
        type={type}
        className="form-control"
        {...input} />
      <div className="form-control-feedback">
        {touched && error}
      </div>
    </div>
  )
}

ValidatableFormField.propTypes = {
  Field: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  meta: PropTypes.object.isRequired
};

export default ValidatableFormField;
