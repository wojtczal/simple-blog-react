import React, { Component } from 'react';
import PostFormContainer from '../containers/PostFormContainer';
import { connect } from 'react-redux';
import { updatePost } from '../actions';

class PostEdit extends Component {
  handleFormSubmit (post) {
    post.id = Number(this.props.params.id);
    this.props.updatePost(post);
  }
  
  render () {
    return (
      <PostFormContainer
        className="post-edit"
        initialValues={this.props.post}
        header="Edit Post"
        onFormSubmit={this.handleFormSubmit.bind(this)} />
    );
  }
}

function mapStateToProps({ posts }, ownProps) {
  const post = posts.all.find(post => post.id === Number(ownProps.params.id));
  return { post };
}

export default connect(mapStateToProps, { updatePost })(PostEdit);
