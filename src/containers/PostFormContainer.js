import React, { Component, PropTypes } from 'react';
import PostForm from '../components/PostForm';
import { withRouter } from 'react-router';
import { reduxForm, Field } from 'redux-form';
import ValidatableFormField from '../components/ValidatableFormField';

class PostFormContainer extends Component {
  handleFormSubmit (post) {
    this.props.onFormSubmit(post);
    this.props.router.push('/posts');
  }
  
  render () {
    return (
      <PostForm
        header={this.props.header}
        onSubmit={this.props.handleSubmit(this.handleFormSubmit.bind(this))}>
        <Field
          Field="input"
          name="title"
          type="text"
          component={ValidatableFormField}
          label="Title" />
        <Field
          Field="textarea"
          name="content"
          component={ValidatableFormField}
          label="Content" />
      </PostForm>
    );
  }
}

PostFormContainer.propTypes = {
  header: PropTypes.string.isRequired,
  onFormSubmit: PropTypes.func.isRequired
}

const validate = values => {
  const errors = {};
  const title = (values.title || '').trim();
  if (!title) {
    errors.title = "Enter a title";
  }
  const content = (values.content || '').trim();
  if (!content) {
    errors.content = "Enter some content";
  }
  return errors;
};

export default reduxForm({
  form: 'PostForm',
  validate
})(withRouter(PostFormContainer));
