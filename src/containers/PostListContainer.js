import React, { Component } from 'react';
import PostList from '../components/PostList';
import { connect } from 'react-redux';
import { deletePost } from '../actions';

class PostListContainer extends Component {
  handlePostRemove (id) {
    if(confirm('Are you sure? This action cannot be reversed')) {
      this.props.deletePost(id);
    }
  }
  
  render () {
    return (
      <PostList posts={this.props.posts} onPostRemove={this.handlePostRemove.bind(this)} />
    );
  }
}

function mapStateToProps(state) {
  const regex = new RegExp(state.posts.filter, 'gi');
  const posts = state.posts.all.filter(post => post.title.match(regex));
  return { posts };
}

export default connect(mapStateToProps, { deletePost })(PostListContainer);
