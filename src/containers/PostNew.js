import React, { Component } from 'react';
import PostFormContainer from '../containers/PostFormContainer';
import { connect } from 'react-redux';
import { createPost } from '../actions';

class PostNew extends Component {
  handleFormSubmit(post) {
    post.id = new Date().valueOf();
    post.title = post.title.trim();
    post.content = post.content.trim();
    this.props.createPost(post);
  }
  
  render () {
    return (
      <PostFormContainer
        className="post-new"
        header="New Post"
        onFormSubmit={this.handleFormSubmit.bind(this)} />
    );
  }
}

export default connect(null, { createPost })(PostNew);
