import React, { Component } from 'react';
import { connect } from 'react-redux';
import SearchForm from '../components/SearchForm';
import { updatePostFilter } from '../actions';

class SearchFormContainer extends Component {
  handleInputChange (e) {
    this.props.updatePostFilter(e.target.value);
  }
  
  render () {
    return (
      <SearchForm value={this.props.filter} onChange={this.handleInputChange.bind(this)} />
    );
  }
}

function mapStateToProps({ posts: { filter } }) {
  return { filter };
}

export default connect(mapStateToProps, { updatePostFilter })(SearchFormContainer);
