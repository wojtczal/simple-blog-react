import * as types from '../actions/types';
const INITIAL_STATE = { all: [], filter: '' };

export default function (state = INITIAL_STATE, action) {
  switch(action.type) {
    case types.CREATE_POST:
      return {...state, all: [...state.all, action.payload]};
    case types.UPDATE_POST:
      const stateWithoutPost = state.all.filter(post => post.id !== action.payload.id);
      return {...state, all: [...stateWithoutPost, action.payload]};
    case types.DELETE_POST:
      const posts = state.all.filter(post => post.id !== action.payload);
      return {...state, all: posts};
    case types.UPDATE_FILTER:
      return {...state, filter: action.payload};
  }
  return state;
}
