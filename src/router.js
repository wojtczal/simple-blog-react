import React from 'react';
import { Router, Route, browserHistory, IndexRoute, IndexRedirect } from 'react-router';
import MainLayout from './components/MainLayout';
import PostIndex from './components/PostIndex';
import PostNew from './containers/PostNew';
import PostEdit from './containers/PostEdit';

export default (
  <Router history={browserHistory}>
    <Route path="/" component={MainLayout}>
      <IndexRedirect to="posts" />
      <Route path="posts">
        <IndexRoute component={PostIndex} />
        <Route path="new" component={PostNew} />
        <Route path=":id/edit" component={PostEdit} />
      </Route>
    </Route>
  </Router>
);
